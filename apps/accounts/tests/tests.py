from apps.accounts.models import CustomUser
from rest_framework.test import APITestCase


class CustomUserTestCase(APITestCase):
    def test_create_user_manager(self):
        result = CustomUser.objects.create_user(
            email="create_user@ex.com", password="sadasdas"
        )
        self.assertEqual(result.email, "create_user@ex.com")
        self.assertFalse(result.is_staff)

    def test_create_superuser_manager(self):
        result = CustomUser.objects.create_superuser(
            email="admin@ex.com", password="sadasdas"
        )
        self.assertEqual(result.email, "admin@ex.com")
        self.assertTrue(result.is_superuser)

    def test_raise_error_when_no_email(self):
        self.assertRaises(
            ValueError,
            CustomUser.objects.create_user,
            email="",
            username="ssssss",
            password="dfsdfsdf",
        )
        self.assertRaisesMessage(ValueError, "The Email must be set")
