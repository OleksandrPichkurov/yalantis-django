from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic.edit import FormView, View
from django.urls import reverse_lazy
from .forms import AuthenticationForm, SignUpForm


class SignUp(FormView):
    form_class = SignUpForm
    template_name = "account/signup.html"
    success_url = reverse_lazy("account:login")

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.save()
        return super().form_valid(form)


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = "account/login.html"

    def form_valid(self, form) -> HttpResponse:
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username=username, password=password)

        if user is not None:
            login(self.request, user)
            messages.success(self.request, "whoosh! and you're at home")
            return redirect("index")
        else:
            return HttpResponse("Wrong credintials")


class LogoutView(View):
    def get(self, request) -> HttpResponse:
        logout(request)
        return redirect("index")
