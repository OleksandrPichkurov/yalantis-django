from django.db import models


class MessageQuerySet(models.QuerySet):
    def read(self):
        return self.filter(is_read=True)

    def unread(self):
        return self.filter(is_read=False)


class MessgeManager(models.Manager):
    def get_queryset(self):
        return MessageQuerySet(self.model, using=self._db)

    def read(self):
        return self.get_queryset().read()

    def unread(self):
        return self.get_queryset().unread()
