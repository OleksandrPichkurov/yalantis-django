from rest_framework import routers

from .views import MessageModelViewSet, ThreadModelViewSet

# Required variable for Django routing
app_name = "dialogs"

router = routers.DefaultRouter()

router.register("threads", ThreadModelViewSet, basename="threads")
router.register("messages", MessageModelViewSet, basename="messages")

urlpatterns = router.urls
