from rest_framework import permissions


class IsOwnerThread(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.participants.filter(email=request.user).exists()


class IsOwnerMessage(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.sender == request.user
