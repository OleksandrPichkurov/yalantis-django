from django.contrib import admin

from .models import Message, Thread


class ThreadAdmin(admin.ModelAdmin):
    date_hierarchy = "created_at"
    list_display = (
        "id",
        "title",
        "participants_list",
        "created_at",
        "updated_at",
    )
    list_filter = ("created_at",)
    search_fields = ("participants__email", "created_at", "title")
    ordering = ("created_at",)
    exclude = ("updated_at",)
    raw_id_fields = ("participants",)

    def participants_list(self, obj):
        return tuple(obj.participants.values_list("username", flat=True))


class MessageAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "text",
        "sender",
        "thread",
        "is_read",
        "created_at",
        "updated_at",
    )
    search_fields = ("sender__email", "thread__title", "is_read")
    date_hierarchy = "created_at"
    ordering = ("created_at",)
    exclude = ("updated_at",)
    raw_id_fields = ("sender", "thread")
    list_select_related = True


admin.site.register(Message, MessageAdmin)
admin.site.register(Thread, ThreadAdmin)
