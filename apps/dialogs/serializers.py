from typing import Dict, List

from django.db.models import Max, QuerySet
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from .models import Message, Thread


class ThreadSerializer(serializers.ModelSerializer):
    unread_messages = SerializerMethodField()
    last_message = SerializerMethodField()

    class Meta:
        model = Thread
        fields = (
            "id",
            "title",
            "participants",
            "last_message",
            "unread_messages",
        )

    def get_unread_messages(self, obj) -> int:
        return obj.thread_messages.unread().count()

    def get_last_message(self, obj) -> Dict:
        return obj.thread_messages.aggregate(last_message_in=Max("created_at"))

    def validate_participants(self, participants) -> List[QuerySet]:
        if len(participants) > 2:
            raise serializers.ValidationError(
                "That thread only for 2 participants."
            )
        return participants


class ThreadDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thread
        fields = (
            "title",
            "participants",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("participants",)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("id", "text", "is_read", "thread", "sender")
        read_only_fields = ("is_read",)

    def validate(self, data) -> str:
        """Only participants in thread can post messages

        Args:
            data : Model field - sender

        Raises:
            serializers.ValidationError: if sender not in thread

        Returns:
            str: sender
        """
        thread_participants = Thread.objects.get(id=data["thread"].id)
        if data["sender"] not in thread_participants.participants.all():
            raise serializers.ValidationError("No Chiting blin")
        return data


class MessageDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("id", "text", "is_read")
