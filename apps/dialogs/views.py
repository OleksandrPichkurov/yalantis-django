from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Message, Thread
from .permissions import IsOwnerMessage, IsOwnerThread
from .serializers import (
    MessageDetailSerializer,
    MessageSerializer,
    ThreadDetailSerializer,
    ThreadSerializer,
)


class ThreadModelViewSet(viewsets.ModelViewSet):

    queryset = Thread.objects.all()
    pagination_class = LimitOffsetPagination
    permission_classes = [IsAuthenticated, IsOwnerThread]
    serializer_class = ThreadSerializer

    def get_serializer_class(self):
        if self.action not in ("list", "create"):
            return ThreadDetailSerializer
        return super().get_serializer_class()

    def destroy(self, request, *args, **kwargs) -> Response:
        """Remove participants from thread and delete thread if not participans
        Returns:
            [Respone]: [204 if successful]
        """
        instance = self.get_object()
        instance.participants.remove(self.request.user.id)
        if not instance.participants.filter(email=request.user).exists():
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(
        methods=["GET"],
        detail=True,
        url_path="messages-read",
        url_name="messages-read",
    )
    def messages_read(self, request, pk: int = None) -> Response:
        """Make is_read = True in thread messages if user read them

        Args:
            pk (int): thread id. Defaults to None.

        Raises:
            NotFound: if no messages

        Returns:
            Response: {"success": True} and HTTP_200_OK
        """
        try:
            messages = Message.objects.filter(thread__id=pk)
        except Message.DoesNotExist:
            raise NotFound({"message": "does not exist"})
        messages_read = []
        for message in messages:
            message.is_read = True
            messages_read.append(message)
        Message.objects.bulk_update(messages_read, ["is_read"])
        return Response(
            {"success": True},
            status=status.HTTP_200_OK,
        )


class MessageModelViewSet(viewsets.ModelViewSet):

    queryset = Message.objects.all()
    pagination_class = LimitOffsetPagination
    permission_classes = [IsAuthenticated, IsOwnerMessage]
    serializer_class = MessageSerializer

    def get_serializer_class(self):
        if self.action not in ("list", "create"):
            return MessageDetailSerializer
        return super().get_serializer_class()

    def list(self, request, *args, **kwargs) -> Response:
        """Get messages list from thread by id

        Args:
            request (?thread=thread_id): getting thred messages

        Returns:
            Response: serializer.data
        """
        try:
            thread_id = self.request.query_params.get("thread", None)
        except request.exception.RequestException as e:
            raise ValueError(e)
        if not thread_id:
            return Response(
                data="Provide ?thread=thread_id, if you want to see thread messages or just create the new one.",  # noqa: E501
                status=status.HTTP_400_BAD_REQUEST,
            )

        queryset = Message.objects.filter(thread__id=thread_id)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
