from apps.dialogs.models import Message, Thread
from apps.dialogs.serializers import ThreadSerializer
from apps.dialogs.tests.factories import (
    MessageFactory,
    ThreadFactory,
    UserFactory,
)
from rest_framework import status
from rest_framework.test import APITestCase

PASSWORD = "testpass"


class ThreadTests(APITestCase):
    def setUp(self):

        self.participants = UserFactory()
        self.thread = ThreadFactory(participants=(self.participants,))
        self.sender = UserFactory()
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()
        self.threads_url = "http://0.0.0.0:8000/api/v1/dialogs/threads/"

    def test_list_threads_without_authenteficated(self):
        # get threads list without authenteficated
        response = self.client.get(self.threads_url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_threads_with_authenteficated_user(self):
        # get threads list with authenteficated
        self.client.login(username=self.user_2.username, password=PASSWORD)
        response = self.client.get(self.threads_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.thread.title)

    def test_post_thread_without_authenteficated(self):
        # post thread without authenteficated
        data = {"title": "lala", "participants": 1}
        response = self.client.post(self.threads_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_thread_with_authenteficated_user(self):
        # post thread
        data = {
            "title": self.thread.title,
            "participants": [self.user_1.id, self.user_2.id],
        }
        self.client.login(username=self.user_1.username, password=PASSWORD)
        response = self.client.post(self.threads_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            Thread.objects.get(id=self.thread.id).title, data["title"]
        )

    def test_post_thread_validate_more_then_2_participants(self):
        # post thread with more then 2 participants
        user_3 = UserFactory()
        data = {
            "title": self.thread.title,
            "participants": [self.user_1.id, self.user_2.id, user_3.id],
        }
        self.client.login(username=self.user_1.username, password=PASSWORD)
        response = self.client.post(self.threads_url, data=data)
        serializer = ThreadSerializer(data=response.data)
        self.assertEqual(serializer.is_valid(), False)
        self.assertEqual(
            response.data,
            {"participants": ["That thread only for 2 participants."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_detail_thread_without_permission(self):
        # get threads list with no onwer
        result = self.client.get(
            self.threads_url, kwargs={"id": self.thread.id}
        )
        self.assertEqual(result.status_code, status.HTTP_403_FORBIDDEN)

    def test_detail_thread_with_owner(self):
        # get threads list with onwer
        self.client.login(username=self.user_1.username, password=PASSWORD)
        result = self.client.get(
            self.threads_url, kwargs={"id": self.thread.id}
        )
        self.assertEqual(result.status_code, status.HTTP_200_OK)

    def test_put_thread_without_permission(self):
        # put not owner
        user_3 = UserFactory.build()
        data = {
            "title": "new_title_new",
            "participants": [self.user_1.id, user_3.id],
        }
        self.client.login(username=user_3.username, password=PASSWORD)
        response = self.client.put(
            self.threads_url, kwargs={"id": self.thread.id}, data=data
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put_thread_owner(self):
        # put owner
        thread = ThreadFactory.create(
            participants=(self.user_1.id, self.user_2.id)
        )
        data = {
            "title": "new_title_new",
            "participants": [self.user_1.id, self.user_2.id],
        }
        self.client.login(username=self.user_2.username, password=PASSWORD)
        response = self.client.put(
            f"{self.threads_url}{thread.id}/", data=data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            Thread.objects.get(id=thread.id).title, data["title"]
        )

    def test_put_thread_without_authenteficated(self):
        # put thread without authenteficated
        data = {"title": "title3"}
        response = self.client.put(
            self.threads_url, kwargs={"pk": self.thread.id}, data=data
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_thread_not_authenteficated_user(self):
        # delete thread without authenteficated
        response = self.client.delete(
            self.threads_url, kwargs={"pk": self.thread.id}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_thread_not_permission(self):
        # delete not owner
        user_3 = UserFactory()
        self.client.login(username=user_3.username, password=PASSWORD)
        response = self.client.delete(f"{self.threads_url}{self.thread.id}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_thread_with_owner(self):
        # delete thread  with owner
        thread = ThreadFactory.create(
            participants=(self.user_1.id, self.user_2.id)
        )
        self.client.login(username=self.user_2.username, password=PASSWORD)
        response = self.client.delete(f"{self.threads_url}{thread.id}/")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            Thread.objects.filter(id=thread.id)
        )


class MessagesTests(APITestCase):
    def setUp(self):

        self.participants = UserFactory()
        self.thread = ThreadFactory(participants=(self.participants,))
        self.sender = UserFactory()
        self.message = MessageFactory(sender=self.sender, thread=self.thread)
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()
        self.messages_url = "http://0.0.0.0:8000/api/v1/dialogs/messages/"

    def test_post_message(self):
        # post message if user in thread)
        thread = ThreadFactory.create(participants=(self.user_1, self.user_2))
        data = {
            "text": self.message,
            "sender": self.user_2.id,
            "thread": thread.id,
        }
        self.client.login(username=self.user_2.username, password=PASSWORD)
        response = self.client.post(self.messages_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["text"], data["text"].text)

    def test_post_message_sender_not_in_thread(self):
        thread = ThreadFactory.create(participants=(self.user_1, self.user_2))
        user_3 = UserFactory()
        self.client.login(username=self.user_2.username, password=PASSWORD)
        data = {
            "text": self.message,
            "sender": user_3.id,
            "thread": thread.id,
        }
        response = self.client.post(self.messages_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_messages_read_by_user(self):
        thread = ThreadFactory.create(participants=(self.user_1, self.user_2))
        message = MessageFactory.create(sender=self.user_2, thread=thread)
        self.client.login(username=self.user_2.username, password=PASSWORD)
        response = self.client.get(
            f"http://0.0.0.0:8000/api/v1/dialogs/threads/{thread.id}/messages-read/"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"success": True})
        self.assertTrue(Message.objects.get(id=message.id).is_read, True)

    def test_thread_messages_list_with_params(self):
        self.client.login(username=self.user_1.username, password=PASSWORD)
        response = self.client.get(
            f"{self.messages_url}?thread={self.thread.id}"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def test_thread_messages_list_with_no_params(self):
        self.client.login(username=self.user_1.username, password=PASSWORD)
        response = self.client.get(f"{self.messages_url}?thread=")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_read_messages_manager(self):
        result = Message.objects.unread()
        self.assertEqual(result.exists(), True)

    def test_unread_messages_manager(self):
        result = Message.objects.read()
        self.assertEqual(result.count(), 0)
