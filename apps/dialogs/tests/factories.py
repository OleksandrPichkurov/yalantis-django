import factory
from apps.accounts.models import CustomUser
from apps.dialogs.models import Message, Thread
from faker import Factory

faker = Factory.create()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CustomUser

    email = factory.LazyAttribute(lambda o: "%s@ex.com" % o.username)
    username = factory.Sequence(lambda n: "test%s" % n)
    password = factory.PostGenerationMethodCall("set_password", "testpass")


class ThreadFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Thread

    title = factory.Faker("sentence", nb_words=4)
    # participants = factory.SubFactory(UserFactory)

    @factory.post_generation
    def participants(self, create, extracted, **kwargs):
        if not create or not extracted:
            # Simple build, or nothing to add, do nothing.
            return

        # Add the iterable of groups using bulk addition
        self.participants.set(extracted)


class MessageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Message

    sender = factory.SubFactory(UserFactory)
    thread = factory.SubFactory(ThreadFactory)
    text = factory.Faker("text")
