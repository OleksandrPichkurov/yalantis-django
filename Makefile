.PHONY: all \
		setup \
		run \
		db \
		black \
		flake8 \
		mypy

venv/bin/activate: ## alias for virtual environment
	python -m venv venv

setup: venv/bin/activate ## project setup
	. venv/bin/activate; pip install pip wheel setuptools
	. venv/bin/activate; pip install -r requirements.txt

run: venv/bin/activate ## Run
	. venv/bin/activate; python manage.py runserver

db: venv/bin/activate ## Run
	. venv/bin/activate; python manage.py migrate

black: venv/bin/activate ## Run black
	. venv/bin/activate; black -l 79 .

flake8: venv/bin/activate ## Run flake
	. venv/bin/activate; flake8 .

mypy: venv/bin/activate ## Run mypy
	. venv/bin/activate; mypy .

test: venv/bin/activate ## Run test
	. venv/bin/activate; docker-compose exec web python manage.py test --failfast

superuser: venv/bin/activate ## Run test
	. venv/bin/activate; docker-compose exec web python manage.py createsuperuser

