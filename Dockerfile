FROM python:3.9-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /yalantis

# install psycopg2 dependencies
RUN  pip install psycopg2-binary

# install dependencies
RUN pip install --upgrade pip
COPY requirements.txt /yalantis/
RUN pip install -r requirements.txt

COPY . /yalantis/
