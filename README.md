## PreRequirements
Make sure you have already installed both Docker Engine and Docker Compose. 
You don’t need to install Python or PostgreSQL, as both are provided by Docker images.

```
$ docker -v
Docker version 20.10.7, build f0df350
$ docker-compose -v
docker-compose version 1.28.4
```

## Setup
To run this project, clone it locally:

And finally:
```
$ docker-compose up --build
```

Add superuser:
```
docker-compose exec web python manage.py createsuperuser

```
Run test:
```
docker-compose exec web python manage.py test
```



##  Url examples for site discovery :
* accounts/login/
* accounts/signup/
* api/v1/dialogs/threads# list of threads and create
```
Example:
{
    "title": "title",
    "participants": 1,2
}
```
* api/v1/dialogs/threads/<int:thread_id> # thread details put, patch, delete
```
{
    "title": "title",
    "participants": 1,2
}
```
* api/v1/dialogs/messages/ # create messages
```
Example:
{
    "text": "text",
    "thread": 1,
    "sender": 1
}
```

* api/v1/dialogs/messages/<int:message_id>/ # message details put, patch, delete
```
Example:
{
    "is_read": True,
    "text": "Change text",
}
```
* api/v1/dialogs/messages/?thread=15 # list of thread messages
* api/v1/dialogs/threads/<int:thread_id>/messages-read # mark all messenges read/unread in single thread
